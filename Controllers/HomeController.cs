﻿using DemoApp_practice.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace DemoApp_practice.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()    
        {
            ViewBag.Title = "Home";
            return View("hero");
        }

        public IActionResult about()
        {
            ViewBag.Title = "about";
            return View();
        }

        public IActionResult resume()
        {
            ViewBag.Title = "resume";
            return View();
        }
        public IActionResult portfolio()
        {
            ViewBag.Title = "portfolio";
            return View();
        }
        public IActionResult services()
        {
            ViewBag.Title = "services";
            return View();
        }


        public IActionResult contact()
        {
            ViewBag.Title = "contact";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}